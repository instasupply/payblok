PayBlok
========

Payblok is a cryptoasset that incentivizes B2B payments within the InstaSupply
platform.

Development
----------

The PayBlok token has been developed internally by the InstaSupply development
team.
