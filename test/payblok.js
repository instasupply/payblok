var PayBlok = artifacts.require("PayBlokToken");

contract('PayBlok', function(accounts) {

    it("should put 250000000000000000000000000 PayBlok in the first account", function() {
        return PayBlok.deployed()
        .then(function(instance) {
            return instance.balanceOf(accounts[0]);
        })
        .then(function(balance) {
            assert.equal(balance.valueOf(), 250000000000000000000000000, "250000000000000000000000000 wasn't in the first account");
        });
    });

    it("should send PayBlok between accounts correctly", function() {
        var payblok_instance;

        var account_one = accounts[0];
        var account_two = accounts[1];

        var accout_one_starting_balance;
        var accout_two_starting_balance;

        var accout_one_ending_balance;
        var accout_two_ending_balance;

        var amount = 10;

        return PayBlok.deployed()
        .then(function(instance) {
            payblok_instance = instance;
            return payblok_instance.balanceOf(account_one);
        })
        .then(function(balance) {
            account_one_starting_balance = balance.toNumber();
            return payblok_instance.balanceOf(account_two);
        })
        .then(function(balance) {
            account_two_starting_balance = balance.toNumber();
            return payblok_instance.transfer(account_two, amount, {from: account_one});
        })
        .then(function() {
            return payblok_instance.balanceOf(account_one);
        })
        .then(function(balance) {
            account_one_ending_balance = balance.toNumber();
            return payblok_instance.balanceOf(account_two);
        })
        .then(function(balance) {
            account_two_ending_balance = balance.toNumber();

            assert.equal(account_one_ending_balance, account_one_starting_balance - amount, "Amount wasn't taken correctly from the sender");
            assert.equal(account_two_ending_balance, account_two_starting_balance + amount, "Amount wasn't sent correctly to the reciever");
        });
    });

    it("should burn PayBlok tokens correctly for owner", function() {
        var payblok_instance;

        var owner_account = accounts[0];

        var initial_supply;
        var end_supply;
        var amount_to_burn = 5000;

        return PayBlok.deployed()
        .then(function(instance) {
            payblok_instance = instance;
            return payblok_instance.totalSupply();
        })
        .then(function(supply) {
            initial_supply = supply.toNumber();
            payblok_instance.burn(amount_to_burn, {from: owner_account});
        })
        .then(function() {
            return payblok_instance.totalSupply();
        })
        .then(function(supply) {
            end_supply = supply.toNumber();

            assert.equal(end_supply, initial_supply - amount_to_burn, "Supply wasn't burned correctly.");
        });
    });

    it("should not burn PayBlok tokens for non-owner", function() {
        var payblok_instance;

        var non_owner_account = accounts[1];

        return PayBlok.deployed()
        .then(function(instance) {
            payblok_instance = instance;
        })
        .then(function() {
            return payblok_instance.burn(5000, {from: non_owner_account});
        })
        .then(assert.fail)
        .catch(function(error) {
            assert.include(
                error.message,
                'revert',
                'Invalid attempts to burn supply should cause a VM Exception and a revert.'
            )
         });
    });

    it("should assign ownership of contract to creator", function() {
        var payblok_instance;

        var contract_owner;

        var contract_creator = accounts[0];

        return PayBlok.deployed()
        .then(function(instance) {
            payblok_instance = instance;
            return payblok_instance.owner();
        })
        .then(function(owner) {
            contract_owner = owner;
            assert.equal(contract_owner, contract_creator, "Contract ownership wasn't initialised correctly.");
        });
    });

    it("should transfer ownership of contract", function() {
        var payblok_instance;

        var contract_owner;

        var initial_owner_account = accounts[0];
        var new_owner_account = accounts[1];

        return PayBlok.deployed()
        .then(function(instance) {
            payblok_instance = instance;
            return payblok_instance.owner();
        })
        .then(function(owner) {
            contract_owner = owner;
            return payblok_instance.transferOwnership(new_owner_account, {from: initial_owner_account});
        })
        .then(function() {
            return payblok_instance.owner();
        })
        .then(function(owner) {
            contract_owner = owner;
            assert.equal(contract_owner, new_owner_account, "Contract ownership wasn't transferred correctly.");
        });
    });

    it("should not transfer ownership of contract if not done by contract owner", function() {
        var payblok_instance;

        var contract_owner;

        var new_owner_account = accounts[1];
        var invalid_transfer_account = accounts[2];

        return PayBlok.deployed()
        .then(function(instance) {
            payblok_instance = instance;
        })
        .then(function() {
            return payblok_instance.transferOwnership(new_owner_account, {from: invalid_transfer_account});
        })
        .then(assert.fail)
        .catch(function(error) {
            assert.include(
                error.message,
                'revert',
                'Invalid attempts to transfer ownership should cause a VM Exception and a revert.'
            )
         });
    });

});