var HDWalletProvider = require("truffle-hdwallet-provider");
var private = require("./private");

module.exports = {
    networks: {
        ganache: {
            host: "localhost",
            port: 7545,
            network_id: '*'
        },
        ropsten:  {
            provider: function() {
                return new HDWalletProvider(
                    private.mnemonic,
                    "https://ropsten.infura.io/" + private.infuraToken
                );
            },
            network_id: 3,
            gas: 2900000
        },
        live: {
            provider: function() {
                return new HDWalletProvider(
                    private.mnemonic,
                    "https://mainnet.infura.io/" + private.infuraToken
                );
            },
            network_id: 1,
            gas: 2900000
        }
    }
};
