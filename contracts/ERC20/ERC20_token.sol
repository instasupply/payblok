pragma solidity 0.4.23;

import "./ERC20_interface.sol";
import "./../SafeMath.sol";


contract ERC20Token is ERC20Interface {
    using SafeMath for uint;

    uint public totalSupply;

    mapping (address => uint) public balances;
    mapping (address => mapping (address => uint)) public allowed;

    function transfer(address _to, uint _value) external returns (bool success) {
        // Default assumes totalSupply can't be over max (2^256 - 1).
        if (balances[msg.sender] >= _value && _value > 0) {
            balances[msg.sender] -= _value;
            balances[_to] += _value;
            emit Transfer(msg.sender, _to, _value);
            return true;
        } else { return false; }
    }

    function transferFrom(address _from, address _to, uint _value) external returns (bool success) {
        if (balances[_from] >= _value && allowed[_from][msg.sender] >= _value && _value > 0) {
            balances[_to] += _value;
            balances[_from] -= _value;
            allowed[_from][msg.sender] -= _value;
            emit Transfer(_from, _to, _value);
            return true;
        } else { return false; }
    }

    function totalSupply() external constant returns (uint) {
        return totalSupply;
    }

    function balanceOf(address _owner) external constant returns (uint balance) {
        return balances[_owner];
    }

    function approve(address _spender, uint _value) external returns (bool success) {
        allowed[msg.sender][_spender] = _value;
        emit Approval(msg.sender, _spender, _value);
        return true;
    }

    function allowance(address _owner, address _spender) external constant returns (uint remaining) {
        return allowed[_owner][_spender];
    }
}
