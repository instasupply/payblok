pragma solidity 0.4.23;

import "./ERC223/ERC223_token.sol";
import "./SafeMath.sol";
import "./Ownable.sol";


// ----------------------------------------------------------------------------
// PayBlok token contract
//
// Symbol      : PBLK
// Name        : PayBlok
// Total supply: 250000000
// Decimals    : 18
//
// (c) by Tim Huegdon, Peter Featherstone for Prodemic Ltd 2018.
// ----------------------------------------------------------------------------
contract PayBlokToken is ERC223Token, Ownable {
    using SafeMath for uint;

    string public symbol;
    string public name;
    address public contractOwner;
    uint8 public decimals;

    // ------------------------------------------------------------------------
    // Constructor
    // ------------------------------------------------------------------------
    constructor() public {
        symbol = "PBLK";
        name = "PayBlok";
        decimals = 18;
        totalSupply = 250000000000000000000000000;
        balances[msg.sender] = totalSupply;
        emit Transfer(address(0), msg.sender, totalSupply);
    }

    event Burn(address indexed burner, uint256 value);

    /**
    * @dev Burns a specific amount of tokens.
    * @param _value The amount of token to be burned.
    */
    function burn(uint256 _value) public onlyOwner {
        _burn(msg.sender, _value);
    }

    function _burn(address _who, uint256 _value) internal {
        require(_value <= balances[_who]);

        balances[_who] = balances[_who].sub(_value);
        totalSupply = totalSupply.sub(_value);
        emit Burn(_who, _value);
        emit Transfer(_who, address(0), _value);
    }
}
