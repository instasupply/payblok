pragma solidity 0.4.23;


contract ERC223Interface {
    function transfer(address _to, uint _value, bytes _data) external;
    event Transfer(address indexed _from, address indexed _to, uint _value, bytes indexed _data);
}
